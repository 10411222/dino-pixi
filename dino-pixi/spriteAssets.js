import spriteList from "./spriteList.js";

const app = new PIXI.Application({
  width: window.innerWidth,
  height: window.innerHeight * 2,
  backgroundColor: 0x1099bb,
  resolution: window.devicePixelRatio || 1,
  transparent: true,
});
document.body.appendChild(app.view);

const container = new PIXI.Container();
app.stage.addChild(container);

let imageURL = "./sprite.png";
PIXI.loader.add("dinoSource", imageURL).load(init);

function init(loader, resources) {
  var dinoCache;
  var dino_replay;
  var dino;
  for (let i = 0; i < spriteList.length; i++) {
    // dinoCache = resources["dinoSource"].texture;
    let base = new PIXI.BaseTexture(imageURL);
    base.setSize(
      spriteList[i].x + spriteList[i].width,
      spriteList[i].y + spriteList[i].height
    );
    dinoCache = new PIXI.Texture(base);
    dino_replay = new PIXI.Rectangle(
      spriteList[i].x,
      spriteList[i].y,
      spriteList[i].width,
      spriteList[i].height
    );
    dinoCache.frame = dino_replay;
    dinoCache.orig = dino_replay;
    // console.log(dinoCache.frame);
    // console.log(dinoCache.orig);
    // console.log(dinoCache._frame);
    dino = new PIXI.Sprite(dinoCache);
    dino.x = 0;
    dino.y = 100 * (i + 1);
    console.log(dino.width);
    app.stage.addChild(dino);
    // console.log(dino);
  }
}
